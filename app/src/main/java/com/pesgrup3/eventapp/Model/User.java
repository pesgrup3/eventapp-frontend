package com.pesgrup3.eventapp.Model;

import com.pesgrup3.eventapp.Utils.DateUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class User {

    private int id;
    private String name;
    private String mail;
    private String photo;
    private Date birthDate;
    private String bio;
    private String phone;
    private String instagram;

    public User() {

    }

    public User(int id, String name, String mail, String photo, Date birthDate, String bio, String phone, String instagram) {

        this.id = id;
        this.name = name;
        this.mail = mail;
        this.photo = photo;
        this.birthDate = birthDate;
        this.bio = bio;
        this.instagram = instagram;
        this.phone = phone;

    }

    public static User parse(String responseData) {
        try {
            JSONObject json = new JSONObject(responseData);
            return new User(
                    json.getInt("id"),
                    json.getString("name"),
                    json.getString("mail"),
                    json.getString("photo"),
                    DateUtils.ISOToDate(json.getString("birth_date")),
                    json.getString("bio"),
                    json.getString("telephone"),
                    json.getString("instagram")
            );
        } catch (JSONException e) {
            e.printStackTrace();
            return new User();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }
}
