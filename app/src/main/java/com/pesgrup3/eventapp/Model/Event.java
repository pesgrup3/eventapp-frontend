package com.pesgrup3.eventapp.Model;

import com.pesgrup3.eventapp.Utils.DateUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class Event {

    private int id;
    private String name;
    private String description;
    private float goal;
    private Date startFundingDate;
    private Date endFundingDate;
    private Date startEventDate;
    private Date endEventDate;
    private Date creationDate;
    private String reports;
    private String location;
    private String creatorName;
    private int creator;
    private boolean isValid;
    private double latitude;
    private double longitude;
    private String photo;
    private ArrayList<String> tags;

    public Event(int id, String name, String description, float goal, Date startFundingDate, Date endFundingDate,
                 Date startEventDate, Date endEventDate, Date creationDate, String reports, String location, int creator,
                 double latitude, double longitude, String photo, ArrayList<String> tags, String creatorName) {

        this.id = id;
        this.name = name;
        this.description = description;
        this.goal = goal;
        this.startFundingDate = startFundingDate;
        this.endFundingDate = endFundingDate;
        this.startEventDate = startEventDate;
        this.endEventDate = endEventDate;
        this.creationDate = creationDate;
        this.reports = reports;
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
        this.creator = creator;
        this.isValid = true;
        this.latitude = latitude;
        this.longitude = longitude;
        this.photo = photo;
        this.tags = tags;
        this.creatorName = creatorName;

    }

    public static Event parse(String responseData) {
        try {
            JSONObject json = new JSONObject(responseData);
            ArrayList<String> tags = new ArrayList<String>();
            JSONArray jArray = json.getJSONArray("tags");
            for (int i = 0; i < jArray.length(); i++) {
                tags.add(jArray.getString(i));
            }
            return new Event(
                    json.getInt("id"),
                    json.getString("name"),
                    json.getString("description"),
                    Float.parseFloat(json.getString("goal")),
                    DateUtils.ISOToDate(json.getString("funding_start_date")),
                    DateUtils.ISOToDate(json.getString("funding_end_date")),
                    DateUtils.ISOToDate(json.getString("event_start_date")),
                    DateUtils.ISOToDate(json.getString("event_end_date")),
                    DateUtils.ISOToDate(json.getString("creation_date")),
                    json.getString("reports"),
                    json.getString("location"),
                    json.getInt("user_creator"),
                    json.getDouble("lat"),
                    json.getDouble("long"),
                    json.getString("photo"),
                    tags,
                    json.getString("user_creator_name")
            );
        } catch (JSONException e) {
            e.printStackTrace();
            return new Event();
        }
    }

    public Event() {
        this.isValid = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public String getShortDescription() {
        return description.length() > 50 ? description.substring(0, 50) + "..." : description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getGoal() {
        return goal;
    }

    public void setGoal(float goal) {
        this.goal = goal;
    }

    public Date getStartFundingDate() {
        return startFundingDate;
    }

    public void setStartFundingDate(Date iniFundingDate) {
        this.startFundingDate = startFundingDate;
    }

    public Date getEndFundingDate() {
        return endFundingDate;
    }

    public void setEndFundingDate(Date endFundingDate) {
        this.endFundingDate = endFundingDate;
    }

    public Date getStartEventDate() {
        return startEventDate;
    }

    public void setStartEventDate(Date startEventDate) {
        this.startEventDate = startEventDate;
    }

    public Date getEndEventDate() {
        return endEventDate;
    }

    public void setEndEventDate(Date endEventDate) {
        this.endEventDate = endEventDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getReports() {
        return reports;
    }

    public void setReports(String reports) {
        this.reports = reports;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public boolean isValid() {
        return isValid;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;

    }

    public String getPhoto() {
        return photo;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public String getCreatorName() {
        return creatorName;
    }
}
