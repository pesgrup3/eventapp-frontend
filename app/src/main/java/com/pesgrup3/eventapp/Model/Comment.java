package com.pesgrup3.eventapp.Model;

import org.json.JSONException;
import org.json.JSONObject;

public class Comment {
    private int commentId;
    private int userId;
    private String userName;
    private String userPhoto;
    private String text;

    public Comment(int commentId, int userId, String userName, String userPhoto, String text) {
        this.commentId = commentId;
        this.userId = userId;
        this.userName = userName;
        this.userPhoto = userPhoto;
        this.text = text;
    }

    public static Comment parse(String responseData) {
        try {
            JSONObject json = new JSONObject(responseData);
            return new Comment(
                    json.getInt("id"),
                    json.getInt("user_id"),
                    json.getString("user_name"),
                    json.getString("user_photo"),
                    json.getString("text")
            );
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getCommentId() {
        return commentId;
    }

    public int getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public String getText() {
        return text;
    }
}