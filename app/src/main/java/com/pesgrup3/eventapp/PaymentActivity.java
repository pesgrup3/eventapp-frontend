package com.pesgrup3.eventapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.pesgrup3.eventapp.Backend.CallsToBackend;

public class PaymentActivity extends AppCompatActivity implements CallsToBackend.FundEventResponse {

    private EditText quantity;
    private EditText numAccount;
    private EditText expiracyDate;
    private EditText cvc;
    private EditText titular;
    private Button payButton;


    private int eventId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        quantity = findViewById(R.id.textViewQuanty);
        numAccount = findViewById(R.id.editTextNumAccount);
        expiracyDate = findViewById(R.id.editTextExpiracyDate);
        cvc = findViewById(R.id.editTextCVC);
        titular = findViewById(R.id.editTextTitular);

        payButton = findViewById(R.id.PayButton);

        Intent intent = getIntent();
        eventId = intent.getIntExtra("eventId", -1);

        payButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                fundEventPetition();
            }
        });

    }

    private void fundEventPetition() {
        if (!checkParams()) return;
        CallsToBackend.getInstance().fundEvent(eventId, Float.parseFloat(quantity.getText().toString()), this, this);
    }

    public void fundEventResponse(boolean result) {
        if (result) {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else fundEventError("");
    }

    public void fundEventError(String error) {
        Toast.makeText(this, getString(R.string.error_more) + error, Toast.LENGTH_SHORT).show();
    }

    private boolean checkParams() {
        if (quantity.getText().toString().isEmpty()) {
            Toast.makeText(this, "Empty fund quantity!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (numAccount.getText().toString().isEmpty()) {
            Toast.makeText(this, "Empty account num!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (expiracyDate.getText().toString().isEmpty()) {
            Toast.makeText(this, "Empty expiracy date!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (cvc.getText().toString().isEmpty()) {
            Toast.makeText(this, "Empty CVC!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (titular.getText().toString().isEmpty()) {
            Toast.makeText(this, "Empty account holder!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}


