package com.pesgrup3.eventapp;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.pesgrup3.eventapp.Utils.SessionUtils;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int id = SessionUtils.getLoggedUserId(getApplicationContext());
        Intent i = new Intent(getApplicationContext(), WelcomeActivity.class);
        if (id != SessionUtils.NOT_LOGGED) {
            i = new Intent(getApplicationContext(), MenuActivity.class);
        }
        startActivity(i);
        finish();
    }
}
