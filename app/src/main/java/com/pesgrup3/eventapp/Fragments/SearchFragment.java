package com.pesgrup3.eventapp.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pesgrup3.eventapp.Adapters.EventAdapter;
import com.pesgrup3.eventapp.Backend.CallsToBackend;
import com.pesgrup3.eventapp.Model.Event;
import com.pesgrup3.eventapp.R;
import com.pesgrup3.eventapp.Utils.DateUtils;
import com.pesgrup3.eventapp.ViewEventActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

public class SearchFragment extends Fragment implements CallsToBackend.EventListResponse {

    private EventAdapter adapter;
    private RecyclerView recyclerView;
    private ArrayList<Event> data = new ArrayList<>();
    private String dataSort;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView textViewNoEvents;
    private String startDate = "";
    private String endDate = "";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search, container, false);

        FloatingActionButton fabSearch = view.findViewById(R.id.fabSearch);

        textViewNoEvents = view.findViewById(R.id.tvSearchNoEvents);

        recyclerView = view.findViewById(R.id.list_view_events);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        adapter = new EventAdapter(data, getContext());
        adapter.setOnItemClickListener(new EventAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Event item, View v) {
                Intent i = new Intent(getContext(), ViewEventActivity.class);
                i.putExtra("eventId", item.getId());
                startActivityForResult(i, 1);
            }
        });
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshEvents);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllEvents();
            }
        });

        fabSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog dialogBuilder = new AlertDialog.Builder(getContext()).create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_search, null);

                final EditText etNameSearch = dialogView.findViewById(R.id.editTextNameSearch);
                final EditText etTagsSearch = dialogView.findViewById(R.id.editTextTagsSearch);

                final TextView tvIniDate = dialogView.findViewById(R.id.tvIniDate);
                final TextView tvEndDate = dialogView.findViewById(R.id.tvEndDate);

                final LinearLayout llDateIniSearch = dialogView.findViewById(R.id.llDateInitSearch);
                final LinearLayout llDateEndSearch = dialogView.findViewById(R.id.llDateEndSearch);

                llDateIniSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Calendar calendar = Calendar.getInstance();
                        int yearNow = calendar.get(Calendar.YEAR);
                        int monthNow = calendar.get(Calendar.MONTH);
                        int dayNow = calendar.get(Calendar.DAY_OF_MONTH);

                        new DatePickerDialog(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, final int year, final int month, final int dayOfMonth) {
                                calendar.set(year, month, dayOfMonth);
                                tvIniDate.setText(DateUtils.calendarToString(calendar));
                                startDate = DateUtils.calendarToISO(calendar);
                            }
                        }, yearNow, monthNow, dayNow).show();
                    }
                });

                llDateEndSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Calendar calendar = Calendar.getInstance();
                        int yearNow = calendar.get(Calendar.YEAR);
                        int monthNow = calendar.get(Calendar.MONTH);
                        int dayNow = calendar.get(Calendar.DAY_OF_MONTH);

                        new DatePickerDialog(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, final int year, final int month, final int dayOfMonth) {
                                calendar.set(year, month, dayOfMonth);
                                tvEndDate.setText(DateUtils.calendarToString(calendar));
                                endDate = DateUtils.calendarToISO(calendar);
                            }
                        }, yearNow, monthNow, dayNow).show();
                    }
                });

                Button btnSearch = dialogView.findViewById(R.id.buttonSubmit);
                Button btnAll = dialogView.findViewById(R.id.buttonAll);

                btnSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // DO SOMETHINGS
                        doSearch(etNameSearch.getText().toString().toLowerCase(), etTagsSearch.getText().toString().toLowerCase());
                        dialogBuilder.dismiss();
                    }
                });
                btnAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // DO SOMETHINGS
                        getAllEvents();
                        dialogBuilder.dismiss();
                    }
                });

                dialogBuilder.setView(dialogView);
                dialogBuilder.show();
            }
        });

        Spinner spinner = view.findViewById(R.id.sort_spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                dataSort = getResources().getStringArray(R.array.order_fields_params)[pos];
                getEventsResponse(data);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        dataSort = getResources().getStringArray(R.array.order_fields_params)[spinner.getSelectedItemPosition()];

        getAllEvents();
        return view;
    }

    private void getAllEvents() {
        swipeRefreshLayout.setRefreshing(true);
        CallsToBackend.getInstance().getAllEvents(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            getAllEvents();
        }
    }

    private void doSearch(String name, String allTags) {
        swipeRefreshLayout.setRefreshing(true);
        CallsToBackend.getInstance().getEventsFiltered(name, allTags, startDate, endDate, this);
        startDate = "";
        endDate = "";
    }

    public void getEventsResponse(ArrayList<Event> data) {
        Collections.sort(data, new Comparator<Event>() {
            @Override
            public int compare(Event item1, Event item2) {
                String[] sortCriteria = dataSort.split(" ");
                int sortRes = sortCriteria[1].equals("asc") ? -1 : 1;
                switch (sortCriteria[0]) {
                    case "date":
                        return item1.getStartEventDate().getTime() < item2.getStartEventDate().getTime() ? sortRes : -sortRes;
                    case "goal":
                        return item1.getGoal() < item2.getGoal() ? sortRes : -sortRes;
                    default:
                        return 0;
                }
            }
        });
        this.data = data;
        updateUI();
    }

    public void getEventsError(String error) {
        Toast.makeText(getContext(), getString(R.string.error_more) + error, Toast.LENGTH_SHORT).show();
    }

    private void updateUI() {
        swipeRefreshLayout.setRefreshing(false);
        textViewNoEvents.setVisibility(View.INVISIBLE);
        adapter = new EventAdapter(data, getContext());
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.setOnItemClickListener(new EventAdapter.ItemClickListener() {
                    @Override
                    public void onItemClick(Event item, View v) {
                        Intent i = new Intent(getContext(), ViewEventActivity.class);
                        i.putExtra("eventId", item.getId());
                        startActivityForResult(i, 1);
                    }
                });
                recyclerView.setAdapter(adapter);
            }
        });

        if (data.isEmpty()) textViewNoEvents.setVisibility(View.VISIBLE);
    }
}