package com.pesgrup3.eventapp.Fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pesgrup3.eventapp.Backend.CallsToBackend;
import com.pesgrup3.eventapp.R;
import com.pesgrup3.eventapp.Utils.DateUtils;
import com.pesgrup3.eventapp.Utils.RESTClient;
import com.pesgrup3.eventapp.Utils.SessionUtils;
import com.pesgrup3.eventapp.ViewEventActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.Security;
import java.util.Calendar;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;

public class CreateEventFragment extends Fragment {

    private EditText eventName;
    private EditText eventDescription;
    private EditText eventGoal;
    private EditText eventLocation;
    private TextView eventStartDate;
    private TextView eventEndDate;
    private TextView eventFundingStartDate;
    private TextView eventFundingEndDate;
    private Calendar eventStartCalendar;
    private Calendar eventEndCalendar;
    private Calendar fundingStartCalendar;
    private Calendar fundingEndCalendar;
    private Button btUploadImage;
    public static final int CHOOSE_IMAGE = 1;
    private ImageView profilePicture;
    private File UPLOADING_IMAGE;
    private String OBJECT_KEY;
    private String urlPicture;
    private EditText eventTags;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_create_event, container, false);

        FloatingActionButton eventCreate = view.findViewById(R.id.fabCreateEvent);

        eventName = view.findViewById(R.id.eventName);
        eventDescription = view.findViewById(R.id.eventDescription);
        eventGoal = view.findViewById(R.id.eventGoal);
        eventLocation = view.findViewById(R.id.eventLocation);

        eventStartDate = view.findViewById(R.id.eventStartDate);
        eventEndDate = view.findViewById(R.id.eventEndDate);
        eventFundingStartDate = view.findViewById(R.id.eventFundingStartDate);
        eventFundingEndDate = view.findViewById(R.id.eventFundingEndDate);

        btUploadImage = view.findViewById(R.id.btImage);
        profilePicture = view.findViewById(R.id.ivImage);

        eventStartCalendar = Calendar.getInstance();
        eventEndCalendar = Calendar.getInstance();
        fundingStartCalendar = Calendar.getInstance();
        fundingEndCalendar = Calendar.getInstance();

        eventTags = view.findViewById(R.id.eventTags);

        SparseArray<Object[]> mapViews = new SparseArray<>();
        mapViews.put(R.id.eventStartDateLayout, new Object[]{eventStartDate, eventStartCalendar});
        mapViews.put(R.id.eventEndDateLayout, new Object[]{eventEndDate, eventEndCalendar});
        mapViews.put(R.id.eventFundingStartDateLayout, new Object[]{eventFundingStartDate, fundingStartCalendar});
        mapViews.put(R.id.eventFundingEndDateLayout, new Object[]{eventFundingEndDate, fundingEndCalendar});

        for (int i = 0; i < mapViews.size(); i++) {
            int key = mapViews.keyAt(i);
            Object[] obj = mapViews.get(key);
            final TextView textView = (TextView) obj[0];
            final Calendar calendar = (Calendar) obj[1];
            textView.setText(DateUtils.calendarToString(calendar));
            view.findViewById(key).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int year = calendar.get(Calendar.YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    int day = calendar.get(Calendar.DAY_OF_MONTH);
                    final int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    final int minute = calendar.get(Calendar.MINUTE);

                    new DatePickerDialog(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, final int year, final int month, final int dayOfMonth) {
                            new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                    calendar.set(year, month, dayOfMonth, selectedHour, selectedMinute);
                                    textView.setText(DateUtils.calendarToString(calendar));
                                }
                            }, hour, minute, true).show();
                        }
                    }, year, month, day).show();
                }
            });
        }

        eventCreate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                uploadImageToServer();
            }
        });

        btUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, CHOOSE_IMAGE);
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == CHOOSE_IMAGE && resultCode == RESULT_OK) {

            Uri uriProfileImage = data.getData();
            profilePicture.setImageURI(uriProfileImage);
        }
    }

    private void postEvent(double latitude, double longitude) {
        JSONObject jsonParam = new JSONObject();
        try {
            jsonParam.put("name", eventName.getText().toString());
            jsonParam.put("description", eventDescription.getText().toString());
            jsonParam.put("event_start_date", DateUtils.calendarToISO(eventStartCalendar));
            jsonParam.put("event_end_date", DateUtils.calendarToISO(eventEndCalendar));
            jsonParam.put("funding_start_date", DateUtils.calendarToISO(fundingStartCalendar));
            jsonParam.put("funding_end_date", DateUtils.calendarToISO(fundingEndCalendar));
            jsonParam.put("goal", eventGoal.getText().toString().isEmpty() ? 0 : Float.parseFloat(eventGoal.getText().toString()));
            jsonParam.put("location", eventLocation.getText().toString());
            jsonParam.put("lat", latitude);
            jsonParam.put("long", longitude);
            jsonParam.put("photo", urlPicture);
            jsonParam.put("tags", new JSONArray(eventTags.getText().toString().split(",")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final ProgressDialog dialog = ProgressDialog.show(getContext(), "", getString(R.string.loading_message), true);
        new RESTClient(CallsToBackend.apiBase + CallsToBackend.apiEvents, jsonParam.toString(), "POST", SessionUtils.getAccessToken(getContext()), new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                dialog.dismiss();
                try {
                    int currEventId = Integer.parseInt(responseData);
                    Intent i = new Intent(getContext(), ViewEventActivity.class);
                    i.putExtra("eventId", currEventId);
                    startActivity(i);
                    getActivity().onBackPressed();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), getString(R.string.error_more) + responseData, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                dialog.dismiss();
                try {
                    String message = (new JSONObject(responseData)).getString("message");
                    Toast.makeText(getContext(), getString(R.string.error_more) + message, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), getString(R.string.error_more) + responseData, Toast.LENGTH_SHORT).show();
                }
            }
        }).execute();
    }

    private void validateLocation() {
        try {
            String search = URLEncoder.encode(eventLocation.getText().toString(), "utf-8");
            new RESTClient("https://api.tomtom.com/search/2/geocode/" + search + ".json?key=e3NAfAiPJkA6ov7cj2GspRB7dL9v944P", "GET", new RESTClient.OnRequestCompleted() {
                @Override
                public void onRequestCompleted(String responseData) {
                    try {
                        JSONObject json = new JSONObject(responseData);
                        JSONArray results = json.getJSONArray("results");
                        if (results.length() > 0) {
                            JSONObject result = results.getJSONObject(0);
                            JSONObject position = result.getJSONObject("position");
                            String address = result.getJSONObject("address").getString("freeformAddress");
                            double lat = position.getDouble("lat");
                            double lon = position.getDouble("lon");
                            eventLocation.setText(address);
                            postEvent(lat, lon);
                        } else {
                            Toast.makeText(getContext(), getString(R.string.location_not_found), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), getString(R.string.geolocation_api) + responseData, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onRequestError(int httpCode, String responseData) {
                    Toast.makeText(getContext(), getString(R.string.geolocation_api) + responseData, Toast.LENGTH_SHORT).show();
                }
            }).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private File imageViewToFile(ImageView image) {
        File f = new File(getContext().getCacheDir(), "picture");
        try {
            f.createNewFile();
            BitmapDrawable bd = (BitmapDrawable) image.getDrawable();
            Bitmap b = bd.getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] bb = baos.toByteArray();
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bb);
            fos.flush();
            fos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return f;
    }

    private void uploadImageToServer() {
        UPLOADING_IMAGE = imageViewToFile(profilePicture);
        OBJECT_KEY = UUID.randomUUID().toString() + ".png";
        AWSCredentials credentials = new BasicAWSCredentials(getString(R.string.AWSAccessKey), getString(R.string.AWSSecretKey));
        AmazonS3 s3 = new AmazonS3Client(credentials);
        Security.setProperty("networkaddress.cache.ttl", "60");
        s3.setRegion(Region.getRegion(Regions.EU_CENTRAL_1));
        s3.setEndpoint("https://s3-eu-central-1.amazonaws.com/");
        TransferUtility transferUtility = new TransferUtility(s3, getContext());
        final TransferObserver observer = transferUtility.upload(getString(R.string.AWSBucketName), OBJECT_KEY, UPLOADING_IMAGE);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state == TransferState.COMPLETED) {
                    urlPicture = "https://" + observer.getBucket() + ".s3.amazonaws.com/" + observer.getKey();
                    validateLocation();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            }

            @Override
            public void onError(int id, Exception ex) {
            }

        });
    }
}