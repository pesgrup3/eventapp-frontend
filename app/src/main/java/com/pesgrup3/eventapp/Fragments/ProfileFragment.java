package com.pesgrup3.eventapp.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pesgrup3.eventapp.AddUserActivity;
import com.pesgrup3.eventapp.Backend.CallsToBackend;
import com.pesgrup3.eventapp.Model.User;
import com.pesgrup3.eventapp.R;
import com.pesgrup3.eventapp.Utils.DateUtils;
import com.pesgrup3.eventapp.Utils.RESTClient;
import com.pesgrup3.eventapp.Utils.SessionUtils;
import com.pesgrup3.eventapp.WelcomeActivity;

public class ProfileFragment extends Fragment implements CallsToBackend.UserResponse {

    private User user;

    private TextView userName;
    private TextView userEmail;
    private TextView userBirth;
    private TextView userBio;
    private TextView userPhone;
    private Button deleteUserButton;
    private FloatingActionButton fab;
    private ImageView userPhoto;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        userName = view.findViewById(R.id.userName);
        userEmail = view.findViewById(R.id.userEmail);
        userBirth = view.findViewById(R.id.userBirth);
        userBio = view.findViewById(R.id.userBio);
        userPhone = view.findViewById(R.id.userPhone);
        deleteUserButton = view.findViewById(R.id.deleteUserButton);
        fab = view.findViewById(R.id.fabProfile);
        userPhoto = view.findViewById(R.id.userPhoto);

        getUser();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), AddUserActivity.class);
                i.putExtra("userId", user.getId());
                startActivityForResult(i, 1);
            }
        });

        deleteUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builderDelete = new AlertDialog.Builder(getContext());
                builderDelete.setMessage(getString(R.string.ask_delete_account))
                        .setTitle(getString(R.string.delete));
                builderDelete.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteUser();
                    }
                });
                builderDelete.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
                AlertDialog dialogDelete = builderDelete.create();
                dialogDelete.show();
            }
        });

        return view;
    }

    private void getUser() {
        CallsToBackend.getInstance().getUser(SessionUtils.getLoggedUserId(getActivity()), this);
    }

    public void getUserResponse(final User user) {
        this.user = user;
        if (user != null)
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    userName.setText(user.getName());
                    userEmail.setText(user.getMail());
                    userBirth.setText(DateUtils.dateToDateString(user.getBirthDate()));
                    userBio.setText(user.getBio());
                    userPhone.setText(user.getPhone());
                    Glide.with(ProfileFragment.this)
                            .load(user.getPhoto())
                            .centerCrop()
                            .apply(RequestOptions.circleCropTransform())
                            .into(userPhoto);
                }
            });
    }

    public void getUserError(String error) {

    }

    private void deleteUser() {
        final ProgressDialog dialog = ProgressDialog.show(getContext(), "", getString(R.string.loading_message), true);
        new RESTClient(CallsToBackend.apiBase + CallsToBackend.apiUsers + user.getId(), "DELETE", new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                dialog.dismiss();
                SessionUtils.deleteLoginData(getContext());
                Intent i = new Intent(getContext(), WelcomeActivity.class);
                startActivity(i);
                getActivity().finish();
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                dialog.dismiss();
                Toast.makeText(getContext(), getString(R.string.delete_user_fail), Toast.LENGTH_SHORT).show();
            }
        }).execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            getUser();
        }
    }

}