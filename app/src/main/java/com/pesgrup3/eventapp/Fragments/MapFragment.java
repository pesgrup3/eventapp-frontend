package com.pesgrup3.eventapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pesgrup3.eventapp.Backend.CallsToBackend;
import com.pesgrup3.eventapp.Model.Event;
import com.pesgrup3.eventapp.R;
import com.pesgrup3.eventapp.ViewEventActivity;

import java.util.ArrayList;

public class MapFragment extends Fragment implements OnMapReadyCallback, CallsToBackend.EventListResponse {

    ArrayList<Event> events;
    private GoogleMap googleMap;
    private FloatingActionButton floatingActionButton;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_map, container, false);
        events = new ArrayList<>();

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.globalMap);
        mapFragment.getMapAsync(this);

        floatingActionButton = view.findViewById(R.id.refreshMap);


        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        UiSettings uiSettings = googleMap.getUiSettings();
        uiSettings.setRotateGesturesEnabled(false);
        getAllEventsPetition();
        iniRefreshButton();
    }

    private void getAllEventsPetition() {
        CallsToBackend.getInstance().getAllEvents(this);
    }

    public void getEventsResponse(ArrayList<Event> data) {
        this.events = data;
        googleMap.clear();
        createMarkers();
    }

    public void getEventsError(String error) {

    }

    private void createMarkers() {
        boolean first = true;
        for (int i = 0; i < events.size(); i++) {

            LatLng latLng = new LatLng(events.get(i).getLatitude(), events.get(i).getLongitude());
            Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng).title(events.get(i).getName()));
            marker.setTag(events.get(i).getId());

            if (first) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
                first = false;
            }
        }

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.showInfoWindow();
                return true;
            }
        });

        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Intent i = new Intent(getContext(), ViewEventActivity.class);
                int id = (int) marker.getTag();
                i.putExtra("eventId", id);
                startActivityForResult(i, 1);
            }
        });

    }

    private void iniRefreshButton() {
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllEventsPetition();
            }
        });
    }
}