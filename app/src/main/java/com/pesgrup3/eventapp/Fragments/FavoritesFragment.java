package com.pesgrup3.eventapp.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.pesgrup3.eventapp.Adapters.EventAdapter;
import com.pesgrup3.eventapp.Backend.CallsToBackend;
import com.pesgrup3.eventapp.Model.Event;
import com.pesgrup3.eventapp.R;
import com.pesgrup3.eventapp.Utils.SessionUtils;
import com.pesgrup3.eventapp.ViewEventActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FavoritesFragment extends Fragment implements CallsToBackend.EventListResponse {

    private EventAdapter adapter;
    private RecyclerView recyclerView;
    private ArrayList<Event> data = new ArrayList<>();
    private String dataSort;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView textViewNoEvents;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_favorites, container, false);

        textViewNoEvents = view.findViewById(R.id.tvSearchNoEvents);

        iniRecyclerView(view);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshEvents);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getFavoriteEvents(SessionUtils.getLoggedUserId(getContext()));
            }
        });

        Spinner spinner = view.findViewById(R.id.sort_spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                dataSort = getResources().getStringArray(R.array.order_fields_params)[pos];
                getEventsResponse(data);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        dataSort = getResources().getStringArray(R.array.order_fields_params)[spinner.getSelectedItemPosition()];

        getFavoriteEvents(SessionUtils.getLoggedUserId(getContext()));
        return view;
    }

    private void iniRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.recyclerViewFavorites);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        adapter = new EventAdapter(data, getContext());
        adapter.setOnItemClickListener(new EventAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Event item, View v) {
                Intent i = new Intent(getContext(), ViewEventActivity.class);
                i.putExtra("eventId", item.getId());
                startActivityForResult(i, 1);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void getFavoriteEvents(int idUser) {
        swipeRefreshLayout.setRefreshing(true);
        CallsToBackend.getInstance().getFavoriteEvents(idUser, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            getFavoriteEvents(SessionUtils.getLoggedUserId(getContext()));
        }
    }

    public void getEventsResponse(ArrayList<Event> data) {
        Collections.sort(data, new Comparator<Event>() {
            @Override
            public int compare(Event item1, Event item2) {
                String[] sortCriteria = dataSort.split(" ");
                int sortRes = sortCriteria[1].equals("asc") ? -1 : 1;
                switch (sortCriteria[0]) {
                    case "date":
                        return item1.getStartEventDate().getTime() < item2.getStartEventDate().getTime() ? sortRes : -sortRes;
                    case "goal":
                        return item1.getGoal() < item2.getGoal() ? sortRes : -sortRes;
                    default:
                        return 0;
                }
            }
        });
        this.data = data;
        updateUI();
    }

    public void getEventsError(String error) {

    }

    private void updateUI() {
        swipeRefreshLayout.setRefreshing(false);
        textViewNoEvents.setVisibility(View.INVISIBLE);
        adapter = new EventAdapter(data, getContext());
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.setOnItemClickListener(new EventAdapter.ItemClickListener() {
                    @Override
                    public void onItemClick(Event item, View v) {
                        Intent i = new Intent(getContext(), ViewEventActivity.class);
                        i.putExtra("eventId", item.getId());
                        startActivityForResult(i, 1);
                    }
                });
                recyclerView.setAdapter(adapter);
            }
        });

        if (data.isEmpty()) textViewNoEvents.setVisibility(View.VISIBLE);
    }
}