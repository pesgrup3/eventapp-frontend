package com.pesgrup3.eventapp.Backend;


import android.content.Context;
import com.pesgrup3.eventapp.Model.Comment;
import com.pesgrup3.eventapp.Model.Event;
import com.pesgrup3.eventapp.Model.User;
import com.pesgrup3.eventapp.Utils.RESTClient;
import com.pesgrup3.eventapp.Utils.SessionUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class CallsToBackend {

    public static String apiBase = "http://event-app.ml:5000/api/";
    public static String apiBase2 = "http://34.89.133.108:5000/api/";
    public static String apiEvents = "events/";
    public static String apiComments = "comments/";
    public static String apiCommentsByEvent = "comments/event/";
    public static String apiUsers = "users/";
    public static String apiWatching = "/watching";
    public static String apiWatch = "/watch";
    public static String apiAuth = "users/auth/";
    public static String apiReport = "report/";
    private static String apiName = "name=";
    private static String apiTags = "tags=";
    private static String apiLocation = "location=";
    private static String apiStartDate = "fromDate=";
    private static String apiEndDate = "toDate=";
    private static String apiVote = "/vote";
    private static String apiVoted = "/voted";
    private static String apiFund = "/fund";
    private static String apiFunded = "/funded";
    private static String apiAcumulated = "/acumulated";

    private static final CallsToBackend ourInstance = new CallsToBackend();

    public static CallsToBackend getInstance() {
        return ourInstance;
    }

    private CallsToBackend() {
    }

    public interface EventResponse {
        void getEventResponse(Event event);

        void getEventError(String error);
    }

    public void getEventById(int id, final EventResponse eventInterface) {
        String url = apiBase + apiEvents + id;
        new RESTClient(url, "GET", new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                Event event = Event.parse(responseData);
                eventInterface.getEventResponse(event);
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                try {
                    String message = (new JSONObject(responseData)).getString("message");
                    eventInterface.getEventError(message);
                } catch (Exception e) {
                    e.printStackTrace();
                    eventInterface.getEventError(responseData);
                }
            }
        }).execute();
    }

    public interface EventListResponse {
        void getEventsResponse(ArrayList<Event> data);

        void getEventsError(String error);
    }

    private static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return value;
        }
    }

    public void getEventsFiltered(String name, String tags, String startDate, String endDate, EventListResponse eventsInterface) {
        String url = apiBase + apiEvents;
        String params = "";
        if (!name.isEmpty()) params += "&" + apiName + encodeValue(name);
        if (!tags.isEmpty()) params += "&" + apiTags + encodeValue(tags);
        if (!startDate.isEmpty()) params += "&" + apiStartDate + encodeValue(startDate);
        if (!endDate.isEmpty()) params += "&" + apiEndDate + encodeValue(endDate);
        if (params.startsWith("&")) params = params.substring(1);
        getEventsRequest(url + "?" + params, "", eventsInterface);
    }

    public void getAllEvents(EventListResponse eventsInterface) {
        String url = apiBase + apiEvents;
        getEventsRequest(url, "", eventsInterface);
    }

    public interface FavoriteEventResponse {
        void isFavoriteEventResponse(boolean result);

        void isFavoriteEventError(String error);

        void createFavEventResponse(boolean result);

        void createFavEventError(String error);

        void deleteFavEventResponse(boolean result);

        void deleteFavEventError(String error);
    }

    public void getFavoriteEvents(final int userId, final EventListResponse eventsInterface) {
        String urlWatching = apiBase + apiUsers + userId + apiWatching;
        new RESTClient(urlWatching, "GET", new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                String ids = "";
                try {
                    JSONObject jsonObject = new JSONObject(responseData);
                    JSONArray jsonArray = jsonObject.getJSONArray("events_watching");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ids += jsonArray.get(i).toString() + ",";
                    }
                    if (jsonArray.length() == 0) {
                        ArrayList<Event> data = new ArrayList<>();
                        eventsInterface.getEventsResponse(data);
                    } else {
                        String urlFavorites = apiBase + apiEvents + "?id=" + ids;
                        getEventsRequest(urlFavorites, "", eventsInterface);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                eventsInterface.getEventsError(responseData);
            }
        }).execute();
    }

    public void isFavoriteEvent(final int userId, final int eventId, final FavoriteEventResponse favoriteEventResponse) {
        String urlWatching = apiBase + apiUsers + userId + apiWatching;
        new RESTClient(urlWatching, "GET", new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                try {
                    JSONObject jsonObject = new JSONObject(responseData);
                    JSONArray jsonArray = jsonObject.getJSONArray("events_watching");
                    boolean fav = false;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        if (jsonArray.get(i).toString().equals(String.valueOf(eventId))) fav = true;
                    }
                    if (jsonArray.length() == 0) {
                        favoriteEventResponse.isFavoriteEventResponse(false);
                    } else {
                        favoriteEventResponse.isFavoriteEventResponse(fav);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                favoriteEventResponse.isFavoriteEventError(responseData);
            }
        }).execute();
    }

    public void createFavoriteEvent(final int eventId, Context context, final FavoriteEventResponse favoriteEventResponse) {
        String urlCreateFav = apiBase + apiEvents + eventId + apiWatch;
        new RESTClient(urlCreateFav, new JSONObject().toString(), "PUT", SessionUtils.getAccessToken(context), new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                favoriteEventResponse.createFavEventResponse(true);
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                favoriteEventResponse.createFavEventError(responseData);
            }
        }).execute();
    }

    public void deleteFavoriteEvent(final int eventId, Context context, final FavoriteEventResponse favoriteEventResponse) {
        String urlCreateFav = apiBase + apiEvents + eventId + apiWatch;
        new RESTClient(urlCreateFav, new JSONObject().toString(), "PUT", SessionUtils.getAccessToken(context), new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                favoriteEventResponse.deleteFavEventResponse(true);
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                favoriteEventResponse.deleteFavEventError(responseData);
            }
        }).execute();
    }

    public interface ValoracioEventResponse {
        void getValorationEventResponse(int value);

        void getValorationEventError(String error);

        void createValorationEventResponse(boolean result);

        void createValorationEventError(String error);
    }

    public void getValoracioEvent(final int userId, final int eventId, final ValoracioEventResponse valoracioEventResponse) {
        String urlWatching = apiBase + apiUsers + userId + apiVoted;
        new RESTClient(urlWatching, "GET", new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                try {
                    JSONObject jsonObject = new JSONObject(responseData);
                    JSONArray jsonArray = jsonObject.getJSONArray("events_voting");
                    int valoration = 0;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonEvent = (JSONObject) jsonArray.get(i);
                        int eid = jsonEvent.getInt("event_id");
                        if (eventId == eid) {
                            valoration = jsonEvent.getInt("stars");
                            break;
                        }
                    }
                    valoracioEventResponse.getValorationEventResponse(valoration);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                valoracioEventResponse.getValorationEventError(responseData);
            }
        }).execute();
    }

    public void createValoracioEvent(final int eventId, final int value, Context context, final ValoracioEventResponse valoracioEventResponse) {
        String urlVotes = apiBase + apiEvents + eventId + apiVote;
        JSONObject jsonParam = new JSONObject();
        try {
            jsonParam.put("stars", value);
        } catch (JSONException e) {
            e.printStackTrace();
            valoracioEventResponse.createValorationEventResponse(false);
        }
        new RESTClient(urlVotes, jsonParam.toString(), "PUT", SessionUtils.getAccessToken(context), new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                valoracioEventResponse.createValorationEventResponse(true);
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                valoracioEventResponse.createValorationEventError(responseData);
            }
        }).execute();
    }

    public interface FundEventResponse {
        void fundEventResponse(boolean result);

        void fundEventError(String error);
    }

    public void fundEvent(int eventId, float value, Context context, final FundEventResponse fundEventResponse) {
        String urlFund = apiBase + apiEvents + eventId + apiFund;
        JSONObject jsonParam = new JSONObject();
        try {
            jsonParam.put("fund_amount", value);
        } catch (JSONException e) {
            e.printStackTrace();
            fundEventResponse.fundEventResponse(false);
        }
        new RESTClient(urlFund, jsonParam.toString(), "PUT", SessionUtils.getAccessToken(context), new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                fundEventResponse.fundEventResponse(true);
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                fundEventResponse.fundEventError(responseData);
            }
        }).execute();
    }


    public interface FundInfoResponse {
        void isFundedEventResponse(boolean result);

        void isFundedEventError(String error);

        void getAcumulatedEventResponse(double value);

        void getAcumulatedEventError(String error);
    }

    public void isFundedEvent(int userId, final int eventId, final FundInfoResponse fundInfoResponse) {
        String urlFunded = apiBase + apiUsers + userId + apiFunded;
        new RESTClient(urlFunded, "GET", new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                try {
                    JSONObject jsonObject = new JSONObject(responseData);
                    JSONArray jsonArray = jsonObject.getJSONArray("events_funding");
                    boolean funded = false;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonEvent = (JSONObject) jsonArray.get(i);
                        int eid = jsonEvent.getInt("event_id");
                        if (eventId == eid) funded = true;
                    }
                    if (jsonArray.length() == 0) {
                        fundInfoResponse.isFundedEventResponse(false);
                    } else {
                        fundInfoResponse.isFundedEventResponse(funded);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                fundInfoResponse.isFundedEventError(responseData);
            }
        }).execute();
    }

    public void getFundedEvents(int userId, final EventListResponse eventListResponse) {
        String urlWatching = apiBase + apiUsers + userId + apiFunded;
        new RESTClient(urlWatching, "GET", new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                String ids = "";
                try {
                    JSONObject jsonObject = new JSONObject(responseData);
                    JSONArray jsonArray = jsonObject.getJSONArray("events_funding");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject aux = (JSONObject) jsonArray.get(i);
                        String id = aux.getString("event_id");
                        ids += id + ",";
                    }
                    if (jsonArray.length() == 0) {
                        ArrayList<Event> data = new ArrayList<>();
                        eventListResponse.getEventsResponse(data);
                    } else {
                        String urlFavorites = apiBase + apiEvents + "?id=" + ids;
                        getEventsRequest(urlFavorites, "", eventListResponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                eventListResponse.getEventsError(responseData);
            }
        }).execute();
    }

    public void getAcumulatedEvent(int eventId, final FundInfoResponse fundInfoResponse) {
        String urlFunded = apiBase + apiEvents + eventId + apiAcumulated;
        new RESTClient(urlFunded, "GET", new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                try {
                    JSONObject jsonObject = new JSONObject(responseData);
                    Double money = jsonObject.getDouble("money");
                    fundInfoResponse.getAcumulatedEventResponse(money);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                fundInfoResponse.getAcumulatedEventError(responseData);
            }
        }).execute();
    }


    private void getEventsRequest(String url, String authToken, final EventListResponse eventsInterface) {
        new RESTClient(url, null, "GET", authToken, new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                ArrayList<Event> data = new ArrayList<>();
                try {
                    JSONArray jsonArray = new JSONArray(responseData);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Event event = Event.parse(jsonArray.getJSONObject(i).toString());
                        data.add(event);
                    }
                    eventsInterface.getEventsResponse(data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                eventsInterface.getEventsError(responseData);
            }
        }).execute();
    }

    public interface UserResponse {
        void getUserResponse(User user);

        void getUserError(String error);
    }

    public void getUser(int userId, final UserResponse userInterface) {
        new RESTClient(CallsToBackend.apiBase + CallsToBackend.apiUsers + userId, "GET", new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                User user = User.parse(responseData);
                userInterface.getUserResponse(user);
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                userInterface.getUserError(responseData);
            }
        }).execute();
    }

    public interface DeleteEventResponse {
        void deleteEventResponse(boolean deleted);
    }

    public void deleteEvent(final int eventId, final DeleteEventResponse deleteEventInterface) {
        new RESTClient(CallsToBackend.apiBase + CallsToBackend.apiEvents + eventId, "DELETE", new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                deleteEventInterface.deleteEventResponse(responseData.equals(String.valueOf(eventId)));
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                deleteEventInterface.deleteEventResponse(false);
            }
        }).execute();
    }

    public interface CommentListResponse {
        void getCommentsResponse(ArrayList<Comment> comments);

        void getCommentsError(String error);
    }

    public void getAllCommentsOfEvent(final int eventId, final CommentListResponse commentsInterface) {
        new RESTClient(CallsToBackend.apiBase + CallsToBackend.apiCommentsByEvent + eventId, "GET", new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                ArrayList<Comment> comments = new ArrayList<>();
                try {
                    JSONArray jsonArray = new JSONArray(responseData);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Comment comment = Comment.parse(jsonArray.getJSONObject(i).toString());
                        comments.add(comment);
                    }
                    commentsInterface.getCommentsResponse(comments);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                commentsInterface.getCommentsError(responseData);
            }
        }).execute();
    }

    public interface DeleteCommentResponse {
        void deleteCommentResponse(boolean deleted);
    }

    public void deleteComment(final int commentId, final DeleteCommentResponse deleteCommentInterface) {
        new RESTClient(CallsToBackend.apiBase + CallsToBackend.apiComments + commentId, "DELETE", new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                deleteCommentInterface.deleteCommentResponse(true);
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                deleteCommentInterface.deleteCommentResponse(false);
            }
        }).execute();
    }

    public interface CreateCommentResponse {
        void createCommentResponse(boolean deleted);
    }

    public void createComment(JSONObject jsonObject, Context context, final CreateCommentResponse createCommentInterface) {
        new RESTClient(CallsToBackend.apiBase + CallsToBackend.apiComments, jsonObject.toString(), "POST", SessionUtils.getAccessToken(context), new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                createCommentInterface.createCommentResponse(true);
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                createCommentInterface.createCommentResponse(false);
            }
        }).execute();
    }

    public interface CreateReportResponse {
        void createReportResponse(boolean reported);
    }

    public void reportEvent(int eventId, String text, Context context, final CreateReportResponse createReportInterface) {
        JSONObject jsonParam = new JSONObject();
        try {
            jsonParam.put("event_id", eventId);
            jsonParam.put("content", text);

            new RESTClient(apiBase + apiEvents + apiReport, jsonParam.toString(), "POST", SessionUtils.getAccessToken(context), new RESTClient.OnRequestCompleted() {
                @Override
                public void onRequestCompleted(String responseData) {
                    createReportInterface.createReportResponse(true);
                }

                @Override
                public void onRequestError(int httpCode, String responseData) {
                    createReportInterface.createReportResponse(false);
                }
            }).execute();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
