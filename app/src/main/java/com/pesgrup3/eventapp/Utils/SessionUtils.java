package com.pesgrup3.eventapp.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.auth0.android.jwt.Claim;
import com.auth0.android.jwt.JWT;

public class SessionUtils {

    public static int NOT_LOGGED = -1;

    public static int getLoggedUserId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("private", Context.MODE_PRIVATE);
        String storedToken = sharedPref.getString("access", "fail");

        try {
            JWT jwt = new JWT(storedToken);
            Claim claim = jwt.getClaim("uid");
            return claim.asInt();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return NOT_LOGGED;
    }

    public static String getAccessToken(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("private", Context.MODE_PRIVATE);
        return sharedPref.getString("access", null);
    }

    public static void saveLoginData(Context context, String access, String refresh) {
        SharedPreferences sharedPref = context.getSharedPreferences("private", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("access", access);
        editor.putString("refresh", refresh);
        editor.apply();
    }

    public static void deleteLoginData(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("private", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove("access");
        editor.remove("refresh");
        editor.apply();
    }
}
