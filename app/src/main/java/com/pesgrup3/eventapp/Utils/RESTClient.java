package com.pesgrup3.eventapp.Utils;

import android.os.AsyncTask;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class RESTClient extends AsyncTask<String, Void, String[]> {

    public interface OnRequestCompleted {
        void onRequestCompleted(String responseData);

        void onRequestError(int httpCode, String responseData);
    }

    private OnRequestCompleted onRequestCompletedListener;
    private String urlStr = "";
    private String query = "";
    private String authToken = "";
    private String method = "GET";

    public RESTClient(String url, String query, String method, String authToken, OnRequestCompleted onRequestCompletedListener) {
        this.urlStr = url;
        this.query = query;
        this.method = method;
        this.authToken = authToken;
        this.onRequestCompletedListener = onRequestCompletedListener;
    }

    public RESTClient(String url, String query, String method, OnRequestCompleted onRequestCompletedListener) {
        this.urlStr = url;
        this.query = query;
        this.method = method;
        this.onRequestCompletedListener = onRequestCompletedListener;
    }

    public RESTClient(String url, String method, OnRequestCompleted onRequestCompletedListener) {
        this.urlStr = url;
        this.method = method;
        this.onRequestCompletedListener = onRequestCompletedListener;
    }

    @Override
    protected String[] doInBackground(String... params) {
        try {
            URL mUrl = new URL(urlStr);
            HttpURLConnection httpConnection = (HttpURLConnection) mUrl.openConnection();
            httpConnection.setRequestMethod(method);
            httpConnection.setRequestProperty("Content-Type", "application/json");
            if (!authToken.isEmpty())
                httpConnection.setRequestProperty("AuthToken", authToken);

            if (method.equals("POST") || method.equals("PUT")) {
                httpConnection.setDoOutput(true);
                OutputStream os = httpConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
            }

            httpConnection.connect();
            BufferedReader br;
            if (httpConnection.getResponseCode() >= HttpURLConnection.HTTP_BAD_REQUEST) {
                br = new BufferedReader(new InputStreamReader(httpConnection.getErrorStream()));
            } else {
                br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
            }
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            if (line != null)
                sb.append(line);
            while ((line = br.readLine()) != null) {
                sb.append("\n").append(line);
            }
            br.close();
            return new String[]{sb.toString(), String.valueOf(httpConnection.getResponseCode())};
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String[] s) {
        super.onPostExecute(s);

        if (onRequestCompletedListener != null) {
            if (s == null) {
                onRequestCompletedListener.onRequestError(-1, null);
            } else if (Integer.parseInt(s[1]) >= HttpURLConnection.HTTP_BAD_REQUEST) {
                onRequestCompletedListener.onRequestError(Integer.parseInt(s[1]), s[0]);
            } else {
                onRequestCompletedListener.onRequestCompleted(s[0]);
            }
        }
    }
}