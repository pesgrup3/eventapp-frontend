package com.pesgrup3.eventapp.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

    public static String calendarToISO(Calendar cal) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        return df.format(cal.getTime());
    }

    public static String calendarToString(Calendar cal) {
        return dateToString(cal.getTime());
    }

    public static String dateToString(Date date) {
        DateFormat df = new SimpleDateFormat("d MMM yyyy HH:mm", Locale.getDefault());
        return df.format(date);
    }

    public static String calendarToDateString(Calendar cal) {
        return dateToDateString(cal.getTime());
    }

    public static String dateToDateString(Date date) {
        DateFormat df = new SimpleDateFormat("d MMM yyyy", Locale.getDefault());
        return df.format(date);
    }

    public static Date ISOToDate(String iso) {
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            df.setTimeZone(TimeZone.getTimeZone("GMT"));
            return df.parse(iso);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String ISOToString(String iso) {
        Date d = ISOToDate(iso);
        return dateToString(d);
    }

    public static String ISOToDateString(String iso) {
        Date d = ISOToDate(iso);
        return dateToDateString(d);
    }

}
