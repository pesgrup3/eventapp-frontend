package com.pesgrup3.eventapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.pesgrup3.eventapp.Utils.SessionUtils;

public class WelcomeActivity extends AppCompatActivity {

    public static final int LOGIN = 1;
    public static final int SIGNUP = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        final Button buttonLogin = findViewById(R.id.buttonLogIn);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LogInActivity.class);
                startActivityForResult(i, LOGIN);
            }
        });

        final Button buttonSignup = findViewById(R.id.buttonSignUp);
        buttonSignup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AddUserActivity.class);
                startActivityForResult(i, SIGNUP);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == LOGIN && SessionUtils.getLoggedUserId(this) != SessionUtils.NOT_LOGGED) {
                Intent i = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(i);
                finish();
            } else if (requestCode == SIGNUP) {
                // TODO: log in new user automatically
                Toast.makeText(this, getString(R.string.account_created), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), LogInActivity.class);
                startActivityForResult(i, LOGIN);
            }
        }
    }
}
