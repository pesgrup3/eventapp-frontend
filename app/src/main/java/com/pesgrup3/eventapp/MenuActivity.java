package com.pesgrup3.eventapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.navigation.NavigationView;
import com.pesgrup3.eventapp.Backend.CallsToBackend;
import com.pesgrup3.eventapp.Fragments.*;
import com.pesgrup3.eventapp.Model.User;
import com.pesgrup3.eventapp.Utils.SessionUtils;

public class MenuActivity extends AppCompatActivity implements CallsToBackend.UserResponse {

    private NavigationView navigationView;
    private DrawerLayout drawerLayout;

    private SearchFragment searchFragment;
    private CreateEventFragment createEventFragment;
    private HistoryFragment historyFragment;
    private FavoritesFragment favoritesFragment;
    private MapFragment mapFragment;
    private ProfileFragment profileFragment;
    private FragmentManager fragmentManager;
    private Fragment fragmentActive;
    private ImageView navHeaderImage;

    private TextView navHeaderName, navHeaderEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_nav_drawer);
        actionBar.setDisplayHomeAsUpEnabled(true);

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        navHeaderName = headerView.findViewById(R.id.navHeaderName);
        navHeaderEmail = headerView.findViewById(R.id.navHeaderEmail);
        navHeaderImage = headerView.findViewById(R.id.navHeaderImageView);

        getUser();

        // TODO: back button doesn't work well
        iniFragments();
        setupNavigationDrawerContent();

        navigationView.getMenu().getItem(0).setCheckable(true);
        navigationView.getMenu().getItem(0).setChecked(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupNavigationDrawerContent() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_search:
                        checkOnlyOne(menuItem);
                        fragmentManager.beginTransaction().hide(fragmentActive).show(searchFragment).addToBackStack("search").commit();
                        fragmentActive = searchFragment;
                        drawerLayout.closeDrawer(GravityCompat.START);
                        return true;
                    case R.id.nav_create:
                        checkOnlyOne(menuItem);
                        fragmentManager.beginTransaction().hide(fragmentActive).show(createEventFragment).addToBackStack("create").commit();
                        fragmentActive = createEventFragment;
                        drawerLayout.closeDrawer(GravityCompat.START);
                        return true;
                    case R.id.nav_history:
                        checkOnlyOne(menuItem);
                        fragmentManager.beginTransaction().hide(fragmentActive).show(historyFragment).addToBackStack("history").commit();
                        fragmentActive = historyFragment;
                        drawerLayout.closeDrawer(GravityCompat.START);
                        return true;
                    case R.id.nav_fav:
                        checkOnlyOne(menuItem);
                        fragmentManager.beginTransaction().hide(fragmentActive).show(favoritesFragment).addToBackStack("favorites").commit();
                        fragmentActive = favoritesFragment;
                        drawerLayout.closeDrawer(GravityCompat.START);
                        return true;
                    case R.id.nav_map:
                        checkOnlyOne(menuItem);
                        fragmentManager.beginTransaction().hide(fragmentActive).show(mapFragment).addToBackStack("map").commit();
                        fragmentActive = mapFragment;
                        drawerLayout.closeDrawer(GravityCompat.START);
                        return true;
                    case R.id.nav_profile:
                        checkOnlyOne(menuItem);
                        fragmentManager.beginTransaction().hide(fragmentActive).show(profileFragment).addToBackStack("profile").commit();
                        fragmentActive = profileFragment;
                        drawerLayout.closeDrawer(GravityCompat.START);
                        return true;
                    case R.id.nav_logout:
                        AlertDialog.Builder builderDelete = new AlertDialog.Builder(MenuActivity.this);
                        builderDelete.setMessage(getString(R.string.ask_logout))
                                .setTitle(getString(R.string.menu_logout));
                        builderDelete.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                logout();
                            }
                        });
                        builderDelete.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });
                        AlertDialog dialogDelete = builderDelete.create();
                        dialogDelete.show();
                        return true;
                }
                return true;
            }
        });
    }

    private void iniFragments() {
        fragmentManager = getSupportFragmentManager();
        searchFragment = new SearchFragment();
        createEventFragment = new CreateEventFragment();
        profileFragment = new ProfileFragment();
        historyFragment = new HistoryFragment();
        favoritesFragment = new FavoritesFragment();
        mapFragment = new MapFragment();

        fragmentActive = searchFragment;

        fragmentManager.beginTransaction().add(R.id.nav_host_fragment, profileFragment, "profile").hide(profileFragment).commit();
        fragmentManager.beginTransaction().add(R.id.nav_host_fragment, mapFragment, "map").hide(mapFragment).commit();
        fragmentManager.beginTransaction().add(R.id.nav_host_fragment, favoritesFragment, "favorites").hide(favoritesFragment).commit();
        fragmentManager.beginTransaction().add(R.id.nav_host_fragment, historyFragment, "history").hide(historyFragment).commit();
        fragmentManager.beginTransaction().add(R.id.nav_host_fragment, createEventFragment, "create").hide(createEventFragment).commit();
        fragmentManager.beginTransaction().add(R.id.nav_host_fragment, searchFragment, "search").commit();
    }

    private void checkOnlyOne(MenuItem menuItem) {
        int size = navigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            navigationView.getMenu().getItem(i).setCheckable(false);
        }

        menuItem.setCheckable(true);
        menuItem.setChecked(true);
    }

    private void checkOnlyOneBack(int item) {
        int size = navigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            navigationView.getMenu().getItem(i).setCheckable(false);
        }

        navigationView.getMenu().getItem(item).setCheckable(true);
        navigationView.getMenu().getItem(item).setChecked(true);
    }

    private void logout() {
        SessionUtils.deleteLoginData(this);
        Intent i = new Intent(getApplicationContext(), WelcomeActivity.class);
        startActivity(i);
        finish();
    }

    private void getUser() {
        CallsToBackend.getInstance().getUser(SessionUtils.getLoggedUserId(this), this);
    }

    public void getUserResponse(final User user) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                navHeaderName.setText(user.getName());
                navHeaderEmail.setText(user.getMail());
                Glide.with(getApplicationContext())
                        .load(user.getPhoto())
                        .centerCrop()
                        .apply(RequestOptions.circleCropTransform())
                        .into(navHeaderImage);

            }
        });
    }

    public void getUserError(String error) {

    }

    @Override
    public void onBackPressed() {
        int count = fragmentManager.getBackStackEntryCount();
        String name;
        if (count == 0) {
            super.onBackPressed();
        } else {
            if (count == 1) {
                name = "search";
            } else {
                name = fragmentManager.getBackStackEntryAt(count - 2).getName();
            }

            Fragment previousFragment = getSupportFragmentManager().findFragmentByTag(name);
            fragmentManager.beginTransaction().hide(fragmentActive).show(previousFragment).commit();
            fragmentManager.popBackStack();
            fragmentActive = previousFragment;

            int item = 0;
            switch (name) {
                case "search":
                    item = 0;
                    break;
                case "create":
                    item = 1;
                    break;
                case "history":
                    item = 2;
                    break;
                case "favorites":
                    item = 3;
                    break;
                case "map":
                    item = 4;
                    break;
                case "profile":
                    item = 5;
                    break;
            }
            checkOnlyOneBack(item);
        }
    }
}
