package com.pesgrup3.eventapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pesgrup3.eventapp.Backend.CallsToBackend;
import com.pesgrup3.eventapp.Model.User;
import com.pesgrup3.eventapp.Utils.DateUtils;
import com.pesgrup3.eventapp.Utils.RESTClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.Security;
import java.util.Calendar;
import java.util.UUID;

public class AddUserActivity extends AppCompatActivity implements CallsToBackend.UserResponse {

    private ImageView userPhoto;
    private EditText userName;
    private EditText userEmail;
    private EditText userPassword;
    private EditText userBirthDate;
    private EditText userPhone;
    private EditText userInstagram;
    private EditText userBio;

    private Calendar birthDateCalendar;

    private FloatingActionButton addUser;
    private ImageButton photoEditButton;

    public static final int CHOOSE_IMAGE = 1;
    private File UPLOADING_IMAGE;
    private String OBJECT_KEY;
    private String urlPicture;

    private int userId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);

        userPhoto = findViewById(R.id.userPhoto);
        userName = findViewById(R.id.userName);
        userEmail = findViewById(R.id.userEmail);
        userPassword = findViewById(R.id.userPassword);
        userBirthDate = findViewById(R.id.userBirth);
        userPhone = findViewById(R.id.userPhone);
        userInstagram = findViewById(R.id.userInstagram);
        userBio = findViewById(R.id.userBio);

        addUser = findViewById(R.id.fabCreateUser);

        photoEditButton = findViewById(R.id.userPhotoEditButton);

        birthDateCalendar = Calendar.getInstance();

        Intent i = getIntent();
        userId = i.getIntExtra("userId", -1);
        if (userId != -1) {
            getUserInfo();
            disableUneditableFields();
        }

        userBirthDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    int year = birthDateCalendar.get(Calendar.YEAR);
                    int month = birthDateCalendar.get(Calendar.MONTH);
                    int day = birthDateCalendar.get(Calendar.DAY_OF_MONTH);

                    new DatePickerDialog(AddUserActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, final int year, final int month, final int dayOfMonth) {
                            birthDateCalendar.set(year, month, dayOfMonth);
                            userBirthDate.setText(DateUtils.calendarToDateString(birthDateCalendar));
                        }
                    }, year, month, day).show();
                }
            }
        });

        addUser.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                uploadImageToServer();
            }
        });

        photoEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, CHOOSE_IMAGE);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CHOOSE_IMAGE && resultCode == RESULT_OK) {
            Uri uriProfileImage = data.getData();
            userPhoto.setImageURI(uriProfileImage);
            UPLOADING_IMAGE = new File(uriProfileImage.getPath().replace("/raw", ""));
        }
    }

    private void getUserInfo() {
        CallsToBackend.getInstance().getUser(userId, this);
    }

    public void getUserResponse(User user) {
        Glide.with(this).load(user.getPhoto()).centerInside().placeholder(R.mipmap.ic_launcher).into(userPhoto);

        userName.setText(user.getName());
        userEmail.setText(user.getMail());
        userPassword.setText("");
        birthDateCalendar.setTime(user.getBirthDate());
        userBirthDate.setText(DateUtils.calendarToDateString(birthDateCalendar));
        userPhone.setText(user.getPhone());
        userInstagram.setText(user.getInstagram());
        userBio.setText(user.getBio());
    }

    public void getUserError(String error) {

    }

    private void userRequest() {
        String url;
        String method;
        JSONObject jsonParam;
        if (userId == -1) {
            url = "";
            method = "POST";
            jsonParam = getJSONToCreate();
        } else {
            url = String.valueOf(userId);
            method = "PUT";
            jsonParam = getJSONToEdit();
        }
        if (!checkParams(jsonParam)) return;
        new RESTClient(CallsToBackend.apiBase + CallsToBackend.apiUsers + url, jsonParam.toString(), method, new RESTClient.OnRequestCompleted() {
            @Override
            public void onRequestCompleted(String responseData) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }

            @Override
            public void onRequestError(int httpCode, String responseData) {
                Toast.makeText(AddUserActivity.this, getString(R.string.error_more) + responseData, Toast.LENGTH_SHORT).show();
            }
        }).execute();
    }

    private JSONObject getJSONToCreate() {
        JSONObject jsonParam = new JSONObject();
        try {
            jsonParam.put("name", userName.getText().toString());
            jsonParam.put("mail", userEmail.getText().toString());
            jsonParam.put("birth_date", DateUtils.calendarToISO(birthDateCalendar));
            jsonParam.put("password", userPassword.getText().toString());
            jsonParam.put("bio", userBio.getText().toString());
            jsonParam.put("telephone", userPhone.getText().toString());
            jsonParam.put("instagram", userInstagram.getText().toString());
            jsonParam.put("photo", urlPicture);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonParam;
    }

    private JSONObject getJSONToEdit() {
        JSONObject jsonParam = new JSONObject();
        try {
            jsonParam.put("name", userName.getText().toString());
            jsonParam.put("mail", userEmail.getText().toString());
            jsonParam.put("birth_date", DateUtils.calendarToISO(birthDateCalendar));
            jsonParam.put("bio", userBio.getText().toString());
            jsonParam.put("telephone", userPhone.getText().toString());
            jsonParam.put("instagram", userInstagram.getText().toString());
            jsonParam.put("photo", urlPicture);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonParam;
    }

    private boolean checkParams(JSONObject jsonParams) {
        try {
            if (jsonParams.getString("name").isEmpty()) {
                Toast.makeText(this, getString(R.string.empty_name), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (jsonParams.getString("mail").isEmpty()) {
                Toast.makeText(this, getString(R.string.empty_mail), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (jsonParams.has("password") && jsonParams.getString("password").isEmpty()) {
                Toast.makeText(this, getString(R.string.empty_passw), Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private File imageViewToFile(ImageView image) {
        File f = new File(getApplicationContext().getCacheDir(), "picture");
        try {
            f.createNewFile();
            BitmapDrawable bd = (BitmapDrawable) image.getDrawable();
            Bitmap b = bd.getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] bb = baos.toByteArray();
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bb);
            fos.flush();
            fos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return f;
    }

    private void disableUneditableFields() {
        userPassword.setEnabled(false);
    }

    private void uploadImageToServer() {
        UPLOADING_IMAGE = imageViewToFile(userPhoto);
        OBJECT_KEY = UUID.randomUUID().toString() + ".png";
        AWSCredentials credentials = new BasicAWSCredentials(getString(R.string.AWSAccessKey), getString(R.string.AWSSecretKey));
        AmazonS3 s3 = new AmazonS3Client(credentials);
        Security.setProperty("networkaddress.cache.ttl", "60");
        s3.setRegion(Region.getRegion(Regions.EU_CENTRAL_1));
        s3.setEndpoint("https://s3-eu-central-1.amazonaws.com/");
        TransferUtility transferUtility = new TransferUtility(s3, getApplicationContext());
        final TransferObserver observer = transferUtility.upload(getString(R.string.AWSBucketName), OBJECT_KEY, UPLOADING_IMAGE);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state == TransferState.COMPLETED) {
                    urlPicture = "https://" + observer.getBucket() + ".s3.amazonaws.com/" + observer.getKey();
                    userRequest();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            }

            @Override
            public void onError(int id, Exception ex) {
            }

        });
    }

}
