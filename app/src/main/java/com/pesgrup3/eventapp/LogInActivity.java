package com.pesgrup3.eventapp;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.pesgrup3.eventapp.Backend.CallsToBackend;
import com.pesgrup3.eventapp.Utils.RESTClient;
import com.pesgrup3.eventapp.Utils.SessionUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class LogInActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText email = findViewById(R.id.userEmail);
        final EditText pass = findViewById(R.id.userPassword);

        Button buttonSignIn = findViewById(R.id.buttonSignIn);

        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String userMail = email.getText().toString();
                final String password = pass.getText().toString();

                if (userMail.length() < 4 || password.length() < 2) {
                    Toast.makeText(LogInActivity.this, getString(R.string.invalid_format), Toast.LENGTH_SHORT).show();
                    return;
                }
                JSONObject jsonParam = new JSONObject();
                try {
                    jsonParam.put("mail", userMail);
                    jsonParam.put("password", password);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                final ProgressDialog dialog = ProgressDialog.show(LogInActivity.this, "", getString(R.string.loading_message), true);
                new RESTClient(CallsToBackend.apiBase + CallsToBackend.apiAuth, jsonParam.toString(), "POST", new RESTClient.OnRequestCompleted() {
                    @Override
                    public void onRequestCompleted(String responseData) {
                        dialog.dismiss();
                        JSONObject completed = null;
                        try {
                            completed = new JSONObject(responseData);
                            String access = completed.getString("access_token");
                            String refresh = completed.getString("refresh_token");
                            SessionUtils.saveLoginData(getApplicationContext(), access, refresh);
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(int httpCode, String responseData) {
                        dialog.dismiss();
                        try {
                            JSONObject printError = new JSONObject(responseData);
                            Toast.makeText(LogInActivity.this, printError.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(LogInActivity.this, getString(R.string.error_more) + responseData, Toast.LENGTH_SHORT).show();
                        }
                    }
                }).execute();
            }
        });
    }

}