package com.pesgrup3.eventapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.pesgrup3.eventapp.Adapters.CommentAdapter;
import com.pesgrup3.eventapp.Backend.CallsToBackend;
import com.pesgrup3.eventapp.Model.Comment;
import com.pesgrup3.eventapp.Model.Event;
import com.pesgrup3.eventapp.Utils.DateUtils;
import com.pesgrup3.eventapp.Utils.SessionUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ViewEventActivity extends AppCompatActivity
        implements CallsToBackend.EventResponse, CallsToBackend.DeleteEventResponse,
        CallsToBackend.CommentListResponse, CallsToBackend.DeleteCommentResponse,
        CallsToBackend.CreateCommentResponse, CallsToBackend.CreateReportResponse,
        CallsToBackend.FavoriteEventResponse, CallsToBackend.ValoracioEventResponse,
        CallsToBackend.FundInfoResponse,
        OnMapReadyCallback {

    public static final int EDIT = 1;
    public static final int PAYMENT = 2;
    private CommentAdapter commentAdapter;
    private RecyclerView recyclerViewComments;
    private ArrayList<Comment> data = new ArrayList<>();

    private TextView description, startDate, endDate, creationDate, goal, creator, location, acumulated;
    private EditText textComment;
    private CollapsingToolbarLayout ctl;
    private FloatingActionButton fab;
    private ProgressDialog progressDialog;
    private RatingBar ratingBar;
    private ImageView imageViewEvent;

    private Button btnJoin;

    private boolean isCreator = false;
    private boolean fav = false;
    private boolean joined = false;
    private boolean serverRate;
    private boolean canRate;

    private int valoration;

    private Event event;
    private int eventId = -1;
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_event);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Intent i = getIntent();
        eventId = i.getIntExtra("eventId", -1);

        iniFields();
        iniCommentsList();
        iniCommentsButton();
        iniRatingBar();
        iniFloatingActionButton();

        getEventRequest();
        getCommentsRequest();
        getValorationPetition();
        getAcumulatedEventPetition();
        isFavoriteEventPetition();
        isFundedEventPetition();

        btnJoin = findViewById(R.id.bntJoin);
        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!joined) {
                    Intent i = new Intent(getApplicationContext(), PaymentActivity.class);
                    i.putExtra("eventId", eventId);
                    startActivityForResult(i, PAYMENT);
                }
            }
        });
    }

    //initializations
    private void iniFields() {
        ctl = findViewById(R.id.toolbar_layout);
        description = findViewById(R.id.tvDescrProp);
        creator = findViewById(R.id.eventCreator);
        location = findViewById(R.id.eventLocation);
        goal = findViewById(R.id.tvGoal);
        startDate = findViewById(R.id.tvDiaInici);
        endDate = findViewById(R.id.tvDiaFi);
        creationDate = findViewById(R.id.tvDiaCreacio);
        textComment = findViewById(R.id.textComment);
        location = findViewById(R.id.eventLocation);
        imageViewEvent = findViewById(R.id.imageViewEvent);
        acumulated = findViewById(R.id.tvAcumulated);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        final NestedScrollView scrollView = findViewById(R.id.scrollViewEvent);
        findViewById(R.id.transparentImage).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_MOVE:
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        return false;
                    case MotionEvent.ACTION_UP:
                        scrollView.requestDisallowInterceptTouchEvent(false);
                        return true;
                    default:
                        return true;
                }
            }
        });
    }

    private void iniCommentsList() {
        recyclerViewComments = findViewById(R.id.list_comments);
        recyclerViewComments.setHasFixedSize(true);
        recyclerViewComments.setLayoutManager(new LinearLayoutManager(ViewEventActivity.this));
        recyclerViewComments.setItemAnimator(new DefaultItemAnimator());

        commentAdapter = new CommentAdapter(data);
        commentAdapter.setOnCommentDeleteListener(new CommentAdapter.CommentDeleteListener() {
            @Override
            public void onCommentDelete(final int commentId, final int userId) {
                AlertDialog.Builder builderDelete = new AlertDialog.Builder(ViewEventActivity.this);
                builderDelete.setMessage(getString(R.string.askIfDeleteComment))
                        .setTitle(getString(R.string.delete));
                builderDelete.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteCommentRequest(commentId);
                    }
                });
                builderDelete.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
                AlertDialog dialogDelete = builderDelete.create();
                dialogDelete.show();
            }
        });

        recyclerViewComments.setAdapter(commentAdapter);
    }

    private void iniCommentsButton() {
        Button btnComment = findViewById(R.id.btn_comment);
        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonParam = new JSONObject();
                try {
                    jsonParam.put("event_id", eventId);
                    jsonParam.put("text", textComment.getText().toString());
                    progressDialog = ProgressDialog.show(ViewEventActivity.this, "", getString(R.string.loading_message), true);
                    createCommentRequest(jsonParam);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void iniRatingBar() {
        ratingBar = findViewById(R.id.ratingBar);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(final RatingBar ratingBar, final float rating, boolean fromUser) {
                if (!serverRate && canRate) {
                    AlertDialog.Builder builderReport = new AlertDialog.Builder(ViewEventActivity.this);
                    builderReport.setMessage(getString(R.string.rate_start) + rating + getString(R.string.rate_end))
                            .setTitle(getString(R.string.rate));
                    builderReport.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button
                            createValorationEventPetition(rating);
                            canRate = false;
                        }
                    });
                    builderReport.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            serverRate = true;
                            ratingBar.setRating(0);
                            serverRate = false;
                        }
                    });
                    AlertDialog dialogReport = builderReport.create();
                    dialogReport.show();
                } else if (!canRate) {
                    serverRate = true;
                    ratingBar.setRating(valoration);
                    serverRate = false;
                }
            }
        });
    }

    private void iniFloatingActionButton() {
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFavEvent();
            }
        });
    }

    //get event
    private void getEventRequest() {
        progressDialog = ProgressDialog.show(this, "", getString(R.string.loading_message), true);
        CallsToBackend.getInstance().getEventById(eventId, this);
    }

    public void getEventResponse(final Event event) {
        progressDialog.dismiss();
        this.event = event;
        googleMap.clear();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ctl.setTitle(event.getName());
                description.setText(event.getDescription());
                creator.setText(event.getCreatorName());
                location.setText(event.getLocation());
                goal.setText("/" + event.getGoal() + "€");
                startDate.setText(DateUtils.dateToString(event.getStartEventDate()));
                endDate.setText(DateUtils.dateToString(event.getEndEventDate()));
                creationDate.setText(DateUtils.dateToString(event.getCreationDate()));

                LatLng latLng = new LatLng(event.getLatitude(), event.getLongitude());
                googleMap.addMarker(new MarkerOptions().position(latLng).title(event.getLocation()));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

                isCreator = SessionUtils.getLoggedUserId(getApplicationContext()) == event.getCreator();
                invalidateOptionsMenu();

                Glide.with(getApplicationContext())
                        .load(event.getPhoto())
                        .centerCrop()
                        .into(imageViewEvent);
            }
        });
    }

    public void getEventError(String error) {
        progressDialog.dismiss();
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        finish();
    }

    //delete event
    private void deleteEventRequest() {
        progressDialog = ProgressDialog.show(this, "", getString(R.string.loading_message), true);
        CallsToBackend.getInstance().deleteEvent(eventId, this);
    }

    public void deleteEventResponse(boolean deleted) {
        progressDialog.dismiss();
        if (deleted) finish();
        else Snackbar.make(getWindow().getDecorView(), getString(R.string.noDeletedEvent), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.action), null).show();
    }

    //getAllComments
    private void getCommentsRequest() {
        CallsToBackend.getInstance().getAllCommentsOfEvent(eventId, this);
    }

    public void getCommentsResponse(ArrayList<Comment> comments) {
        data.clear();
        data = comments;
        commentAdapter = new CommentAdapter(data);
        recyclerViewComments.setAdapter(commentAdapter);
    }

    public void getCommentsError(String error) {
        data.clear();
        commentAdapter = new CommentAdapter(data);
        recyclerViewComments.setAdapter(commentAdapter);
        Toast.makeText(ViewEventActivity.this, getString(R.string.load_comments), Toast.LENGTH_SHORT).show();
        finish();
    }

    //delete comment
    private void deleteCommentRequest(final int commentId) {
        progressDialog = ProgressDialog.show(this, "", getString(R.string.loading_message), true);
        CallsToBackend.getInstance().deleteComment(commentId, this);
    }

    public void deleteCommentResponse(boolean deleted) {
        progressDialog.dismiss();
        if (deleted) {
            getCommentsRequest();
        } else {
            Snackbar.make(getWindow().getDecorView(), getString(R.string.noDeletedComment), Snackbar.LENGTH_LONG)
                    .setAction(getString(R.string.action), null).show();
        }
    }

    //do comment
    private void createCommentRequest(JSONObject jsonObject) {
        CallsToBackend.getInstance().createComment(jsonObject, this, this);
    }

    public void createCommentResponse(boolean created) {
        progressDialog.dismiss();
        if (created) {
            textComment.setText("");
            getCommentsRequest();
        } else {
            Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
        }
    }

    //doReport
    private void createReportPetition() {
        CallsToBackend.getInstance().reportEvent(eventId, "report", this, this);
    }

    public void createReportResponse(final boolean reported) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (reported)
                    Toast.makeText(getApplicationContext(), getString(R.string.event_reported), Toast.LENGTH_SHORT).show();
                else if (reported)
                    Toast.makeText(getApplicationContext(), getString(R.string.event_no_reported), Toast.LENGTH_SHORT).show();
            }
        });
    }


    //check if event is favorite or note
    private void isFavoriteEventPetition() {
        CallsToBackend.getInstance().isFavoriteEvent(SessionUtils.getLoggedUserId(getApplicationContext()), eventId, this);
    }

    public void isFavoriteEventResponse(boolean result) {
        fav = result;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (fav) fab.setImageResource(R.drawable.ic_fav_press);
                else fab.setImageResource(R.drawable.ic_fav_unpress);
            }
        });
    }

    public void isFavoriteEventError(String error) {
        showMessage(getString(R.string.error_more) + error);
    }

    //clicked fav event
    private void onFavEvent() {
        if (fav) {
            AlertDialog.Builder builderReport = new AlertDialog.Builder(this);
            builderReport.setMessage(getString(R.string.delete_favorites))
                    .setTitle(getString(R.string.menu_favorites));
            builderReport.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                    deleteFavEventPetition();
                }
            });
            builderReport.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                }
            });
            AlertDialog dialogReport = builderReport.create();
            dialogReport.show();
        } else {
            AlertDialog.Builder builderReport = new AlertDialog.Builder(this);
            builderReport.setMessage(getString(R.string.add_favorites))
                    .setTitle(getString(R.string.menu_favorites));
            builderReport.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                    createFavEventPetition();
                }
            });
            builderReport.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                }
            });
            AlertDialog dialogReport = builderReport.create();
            dialogReport.show();
        }
    }

    //clicked report event
    private void onReportEvent() {
        AlertDialog.Builder builderReport = new AlertDialog.Builder(this);
        builderReport.setMessage(getString(R.string.askIfReport))
                .setTitle(getString(R.string.report));
        builderReport.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                createReportPetition();
            }
        });
        builderReport.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        AlertDialog dialogReport = builderReport.create();
        dialogReport.show();
    }

    //create fav event
    private void createFavEventPetition() {
        CallsToBackend.getInstance().createFavoriteEvent(eventId, this, this);
    }

    public void createFavEventResponse(boolean result) {
        if (result) {
            fav = true;
            isFavoriteEventPetition();
        } else showMessage(getString(R.string.error));

    }

    public void createFavEventError(String error) {
        showMessage(getString(R.string.error_more) + error);
    }

    //delete fav event
    private void deleteFavEventPetition() {
        CallsToBackend.getInstance().deleteFavoriteEvent(eventId, this, this);
    }

    public void deleteFavEventResponse(boolean result) {
        if (result) {
            fav = false;
            isFavoriteEventPetition();
        } else showMessage(getString(R.string.error));
    }

    public void deleteFavEventError(String error) {
        showMessage(getString(R.string.error_more) + error);
    }

    //valoració
    private void getValorationPetition() {
        CallsToBackend.getInstance().getValoracioEvent(SessionUtils.getLoggedUserId(this), eventId, this);
    }

    public void getValorationEventResponse(final int value) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                valoration = value;
                serverRate = true;
                ratingBar.setRating(value);
                serverRate = false;
                if (value > 0) canRate = false;
                else canRate = true;
            }
        });
    }

    public void getValorationEventError(String error) {
        showMessage(getString(R.string.error_more) + error);
    }

    private void createValorationEventPetition(float value) {
        CallsToBackend.getInstance().createValoracioEvent(eventId, (int) value, this, this);
    }

    public void createValorationEventResponse(boolean result) {
        if (result) {
            getValorationPetition();
        } else {
            getValorationPetition();
            showMessage(getString(R.string.error));
        }
    }

    public void createValorationEventError(String error) {
        getValorationPetition();
        showMessage(getString(R.string.error_more) + error);
    }

    //join
    private void isFundedEventPetition() {
        CallsToBackend.getInstance().isFundedEvent(SessionUtils.getLoggedUserId(getApplicationContext()), eventId, this);
    }

    public void isFundedEventResponse(boolean result) {
        joined = result;
        changeJoinButtonState();
    }

    public void isFundedEventError(String error) {
        showMessage(getString(R.string.error_more) + error);
    }

    //get Acumulated
    private void getAcumulatedEventPetition() {
        CallsToBackend.getInstance().getAcumulatedEvent(eventId, this);
    }

    @Override
    public void getAcumulatedEventResponse(final double value) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                acumulated.setText(String.valueOf(value));
            }
        });
    }

    public void getAcumulatedEventError(String error) {
        showMessage(getString(R.string.error_more) + error);
    }


    //menu options
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        if (isCreator) inflater.inflate(R.menu.menu_view_event, menu);
        else inflater.inflate(R.menu.menu_view_event_no_creator, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                if (event == null) break;
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        event.getName() + "\n\n" + event.getDescription() + "\n\n" +
                                event.getLocation() + "\n" +
                                DateUtils.dateToString(event.getStartEventDate()) + " - " + DateUtils.dateToString(event.getEndEventDate()));
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
                break;

            case R.id.action_report:
                onReportEvent();
                break;

            case R.id.action_delete:
                AlertDialog.Builder builderDelete = new AlertDialog.Builder(this);
                builderDelete.setMessage(getString(R.string.askIfDelete))
                        .setTitle(getString(R.string.delete));
                builderDelete.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteEventRequest();
                    }
                });
                builderDelete.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
                AlertDialog dialogDelete = builderDelete.create();
                dialogDelete.show();
                break;

            case R.id.action_edit:
                Intent i = new Intent(getApplicationContext(), EditEventActivity.class);
                i.putExtra("eventId", eventId);
                startActivityForResult(i, EDIT);
                break;

            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //join button
    private void changeJoinButtonState() {
        if (joined) {
            btnJoin.setBackground(getDrawable(R.drawable.round_rectangle_green));
            btnJoin.setText(getString(R.string.joined));
            btnJoin.setTextColor(getResources().getColor(android.R.color.white));

        } else {
            btnJoin.setBackground(getDrawable(R.drawable.round_rectangle_white));
            btnJoin.setText(getString(R.string.join));
            btnJoin.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == EDIT) {
                getEventRequest();
            } else if (requestCode == PAYMENT) {
                joined = true;
                changeJoinButtonState();
                Toast.makeText(this, getString(R.string.joinEvent), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showMessage(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }
}
