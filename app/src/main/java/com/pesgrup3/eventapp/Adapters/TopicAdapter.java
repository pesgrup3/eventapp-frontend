package com.pesgrup3.eventapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.pesgrup3.eventapp.R;

import java.util.ArrayList;

public class TopicAdapter extends ArrayAdapter<TopicItem> {

    public TopicAdapter(Context context, ArrayList<TopicItem> itemArrayList) {
        super(context, 0, itemArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.text_spinner, parent, false
            );
        }

        ImageView imageTopic = convertView.findViewById(R.id.image_topic);
        TextView nameTopic = convertView.findViewById(R.id.name_topic);

        TopicItem currentItem = getItem(position);

        if (currentItem != null) {
            imageTopic.setImageResource(currentItem.getTopicImage());
            nameTopic.setText(currentItem.getTopicName());
        }

        return convertView;
    }
}