package com.pesgrup3.eventapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pesgrup3.eventapp.Model.Comment;
import com.pesgrup3.eventapp.R;
import com.pesgrup3.eventapp.Utils.SessionUtils;

import java.util.ArrayList;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private ArrayList<Comment> dataSet;
    private static CommentDeleteListener deleteListener;
    private int idLoggedUser;
    private Context context;

    public CommentAdapter(ArrayList<Comment> data) {
        this.dataSet = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_list, parent, false);
        context = parent.getContext();
        idLoggedUser = SessionUtils.getLoggedUserId(context);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textName;
        TextView textViewComment = holder.textComment;
        ImageView imageView = holder.imageViewIcon;
        ImageButton btnDeleteComment = holder.btnDeleteComment;

        holder.id = listPosition;
        textViewName.setText(dataSet.get(listPosition).getUserName());
        textViewComment.setText(dataSet.get(listPosition).getText());
        holder.commentId = dataSet.get(listPosition).getCommentId();
        holder.userId = dataSet.get(listPosition).getUserId();

        Glide.with(context)
                .load(dataSet.get(listPosition).getUserPhoto())
                .centerCrop()
                .apply(RequestOptions.circleCropTransform())
                .into(imageView);

        if (idLoggedUser == holder.userId) btnDeleteComment.setVisibility(View.VISIBLE);
        else btnDeleteComment.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void setOnCommentDeleteListener(CommentDeleteListener deleteListener) {
        this.deleteListener = deleteListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textName;
        TextView textComment;
        ImageView imageViewIcon;
        ImageButton btnDeleteComment;
        int commentId;
        int userId;
        int id;

        public ViewHolder(View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.userName);
            textComment = itemView.findViewById(R.id.textComment);
            imageViewIcon = itemView.findViewById(R.id.imgPhoto);
            btnDeleteComment = itemView.findViewById(R.id.btn_delete_comment);
            btnDeleteComment.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            deleteListener.onCommentDelete(commentId, userId);
        }
    }

    public interface CommentDeleteListener {
        void onCommentDelete(int commentId, int userId);
    }
}
