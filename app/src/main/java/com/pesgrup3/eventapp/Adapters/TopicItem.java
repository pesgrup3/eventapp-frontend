package com.pesgrup3.eventapp.Adapters;

public class TopicItem {
    private String topicname;
    private int mItemImage;

    public TopicItem(String topicName, int itemImage) {
        topicname = topicName;
        mItemImage = itemImage;
    }

    public String getTopicName() {
        return topicname;
    }

    public int getTopicImage() {
        return mItemImage;
    }
}

