package com.pesgrup3.eventapp.Adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.pesgrup3.eventapp.Model.Event;
import com.pesgrup3.eventapp.R;

import java.util.ArrayList;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private ArrayList<Event> dataSet;
    private Context context;
    private static ItemClickListener clickListener;

    public EventAdapter(ArrayList<Event> data, Context context) {
        this.dataSet = data;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        TextView textViewVersion = holder.textViewVersion;
        TextView textViewTags = holder.textViewTags;

        ImageView imageView = holder.imageViewIcon;
        holder.id = listPosition;

        textViewName.setText(dataSet.get(listPosition).getName());
        textViewVersion.setText(dataSet.get(listPosition).getShortDescription());
        Glide.with(context)
                .load(dataSet.get(listPosition).getPhoto())
                .centerCrop()
                .into(imageView);
        String tags = TextUtils.join(", ", dataSet.get(listPosition).getTags());
        textViewTags.setText(tags);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewName;
        TextView textViewVersion;
        TextView textViewTags;
        ImageView imageViewIcon;

        int id;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.name_event);
            textViewVersion = itemView.findViewById(R.id.type_event);
            textViewTags = itemView.findViewById(R.id.tvTagsEvent);
            imageViewIcon = itemView.findViewById(R.id.imgPhoto);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(dataSet.get(id), v);
        }
    }

    public interface ItemClickListener {
        void onItemClick(Event item, View v);
    }
}
